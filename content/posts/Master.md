# Trabajo Daw sobre GIT

## Indice

- [Trabajo Daw sobre GIT](#trabajo-daw-sobre-git)
  - [Indice](#indice)
  - [Manual de GIT](#manual-de-git)
    - [Introdución](#introdución)
    - [Git como la mejor alternativa](#git-como-la-mejor-alternativa)
    - [Como funciona GIT](#como-funciona-git)
    - [Comandos básicos de GIT](#comandos-básicos-de-git)
  - [Manual de GIT-FLOW](#manual-de-git-flow)
    - [Introducción](#introducción)
    - [Como funciona Git Flow](#como-funciona-git-flow)
    - [Ramas en git flow](#ramas-en-git-flow)
    - [Comandos de git-flow](#comandos-de-git-flow)
  - [Integración continua](#integración-continua)
    - [Explicación](#explicación)
    - [Cuando se utiliza](#cuando-se-utiliza)
    - [Ventajas de la CI](#ventajas-de-la-ci)
    - [Retos de la CI](#retos-de-la-ci)
    - [Herramientas CI](#herramientas-ci)
    - [Runners, pipelines y stages](#runners-pipelines-y-stages)

## Manual de GIT
### Introdución
Git es un sistema de control de versiones, que es una herramienta que ayuda a gestionar los diferentes cambios
que se relizan sobre un archivo o archivos, del tipo que sean, a lo largo del tiempo.  

### Git como la mejor alternativa
Git es el sistema de control de versiones más utilizado actualmente. Es un proyecto de código abierto, desarrollado originalmente por Linus Torvalds.
Una enorme multitud de proyectos de software dependen de Git para el control de versiones, como proyectos comerciales y de código abierto. De ahí la importancia de aprender Git.

### Como funciona GIT
Podemos entender GIT como sistema de control de versiones o una especie de base de datos alojada en la nube.

Sobre el cual se pueden realizar cambios, modificar, añadir, eliminar y realizar todas las operaciones
que nos interese.  
Entonces, cada repositorio sería coma una base de datos en la que vamos guardando cosas, como archivos por ejemplo.

El funcionamiento es sencillo de entender. Primero está el repositorio remoto, que sería la copia existente en la nube.
Luego estaría nuestra copia local, sobre la que realizamos los cambios desde nuestro ordenador y, posteriormente, esos
cambios pueden ser subidos mediante una serie de comandos que veremos a continuación.  
También es posible a la inversa, es decir, realizar cambios en el repositorio remoto mediante una interfaz web como github o gitlab, por ejemplo, y luego, también mediante comandos, recuperar esos cambios
para nuestra copia local.

### Comandos básicos de GIT

**- GIT INIT**  
  Es el primer comando que debemos conocer de git. Ejecutando etse comando en una carpeta especifica te iniciará un repositorio GIT en ella, y su control de versiones, el cual se encontrará en una carpeta oculta llamada **.git**.

    -git init 
  Si por otra parte deseamos crear un repositorio vacio ejecutaremos el mismo comando pero añadiremos el nombre del repositorio que querramos crear.
    
    -git init nombreRepositorio

**- GIT CLONE** 

  Se utiliza para apuntar a un repositorio y clonar o copiarlo en un repositorio local, el repositorio a clonar puede estar tanto en remoto como en local 
    
    -git clone nombreRepositorio nombreDelDirectorio

**- GIT ADD**  
  Este comando, metera en *stage*, es decir prepara el archivo o archivos indicados para su posterior subida al repositorio.

    -git add nombreArchivo
  Con la opcion de comando *-A* prepararemos todos los archivos que hayan sido modificados, eliminados y/o añadidos en vez de espedificar cada archivo.
    
    -git add -A

**- GIT COMMIT**  
  Guardará en el repositorio local todos los cambios que se hayan metido en *stage* hasta el momento, donde se almacenan los cambios después de ejecutar *git add* en el repositorio local.  
  A cada commit se le tendrá que añadir un mensaje,  normalmente para informar de los cambios realizados.
    
    -git commit -m "aquí un mensaje"
  Variantes:  

    -git commit -a -m "mensaje"
  Lo que hará será guardar los cambios nuevos en archivos existentes sin necesidad de ejecutar *git add* antes. Pero no funciona con archivos nuevos, deben existir con anterioridad en el repositorio

    -git commit --amend -m "mensaje corregido"
  Cambiará el mensaje del último commit. También es posible añadir un archivo al anterior commit ejecutando antes el comando -git add archivoA

    -git commit --amend --no-edit
  En caso de que queramos añadir algún archivo al commit anterior pero sin cambiar el mensaje.

**- GIT STASH**
  Guarda los cambios realizados hasta ahora y los revierte hasta el ultimo commit realizado, es útil para realizar pruebas rapidas o cambios a los que luego haras commit:

    -git stash
  Para luego recuperar los datos guardados en el stash se utiliza:

    -git stash pop
  Si por otra parte quieres implementar los cambios del stash en el repositorio actual o en otras ramas se utiliza:

    -git stash apply

**- GIT TAG**  
  Crea una etiqueta en el repositorio actual

    -git tag nombreDeLaEtiqueta

  Crea una etiqueta anotada la cual almacena datos de quien la insertó

    -git tag -a v1.4

**- GIT BLAME**  
  Este comando se utiliza esencialmente para observar laa autoria de los cambios de un archivo de git, mostrará los cambios por linea de un archivo del la última persona que haya modificado dicha linea.
    
    -git blame nombreArchivo

**- GIT RM**
  Elimina el archivos en seguimiento del índice de Git, es decir no se actualizara al realizar un commit

    -git rm nombreArchivo
**- GIT PUSH**  
  subira al repositorio remoto todos los archivos guardados en el repositorio local a los cuales se le hayan hecho commit.
 
    -git push
**- GIT FETCH**  
  te muestra si los hay, los cambios realizados en comparacion del último commit, pero no te modifica el repositorio local 

    -git fetch

**- GIT MERGE**  
  Fusiona dos ramas diferentes de GIT creadas por git branch e unirlas en una sola rama.
  Pueden darse varios casos al usarlo:  
  -que funcione sin problemas, es decir que no haya conflictos.
  -que haya conflictos, es decir que existan en ambas ramas algun archivo con el mismo nombre y distinto contenido, en este caso saltara un indicador visual: *<<<<<<<, =======, y >>>>>>>* 

      here is some content not affected by the conflict
      <<<<<<< main
      this is conflicted text from main
      =======
      this is conflicted text from feature branch
      >>>>>>> feature branch;

**- GIT PULL**  
  Este comando es una abreviación de *git fetch* seguido de *git merge*.  
  Con *git fetch* lo que hacemos es recuperar la última copia del repositorio remoto pero, no copiamos nada en el nuestro local, es como si solo comprobáramos si ha habido actulizaciones en el repositorio remoto. Dichos cambios quedan, ahora, a la espera de ejecutar un comando que los añada a nuestro repositorio local, en caso de haberlos, como *git merge*.
    
    -git pull repositorioRemoto

**- GIT BRANCH**  
  Crea una rama del repositorio señalado, es decir crea una linea de desarrollo distinta de la principal, esto se usa habitualmente para pequeñas modificaciones o para pruebas de codigo.

    -git branch nombreRama
  Podemos usar el mismo comando pero sin insertar un nombre de rama y se nos mostraran las ramas de tu repositorio.
    
    -git branch
  Si agregamos -D eliminaremos la rama espedificada

    -git branch -D nombreRama

**- GIT CHECKOUT**  
  Guarda los cambios de la rama actual, y te cambia de rama a la espedificada.

    -git checkout nombreRama
  Si queremos guardar los cambios de la rama actual, crear una nueva rama y cambiarnos a ella usamos:
    
    -git checkout -b nombreRamaNueva

## Manual de GIT-FLOW
https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow

### Introducción   
**Qué es git flow?**  
Git flow es una manera de utilizar git, de organizar el trabajo. Si un proyecto es lo suficientemente grande es recomendable que haya una serie de normas o recomendaciones sobre como organizar el trabajo, las tareas a realizar y cómo, en qué orden y cuándo volver al flujo normal o principal de trabajo. Eso es git flow y cualquier otro modelo de flujo de trabajo con git.  
Es como una extensión del propio git que también hay que instalar, una vez instalado e inicializado un repositorio git. 

**Ventajas**  
Mejor organización del trabajo  

**Desventajas**  

Pueden surgir conflictos entre las versiones de los trabajadores y es necesario arreglar esos conflictos de código antes de seguir lo que puede suponer una pérdida de tiempo importante.  

**Otros flujos de trabajo con git**  
- GitHub Flow
- GitLab Flow
- Master-only Flow  
  

Los 2 primeros funcionan también con distintas ramas solo que con distintas filosofías a la hora de organizar la creación de ramas y su función. La tercera, en cambio, funciona con una única rama.

### Como funciona Git Flow  
El trabajo comienza siempre con una rama principal, la que se crea automáticamente al crear el repositorio, partimos de esa rama y vamos creando las demás dependiendo de cuando las vayamos necesitando. No volverán a unirse a la rama principal hasta que se termine el trabajo.

### Ramas en git flow
Entonces, partiendo de la rama main, primero tendremos la rama develop y es a partir de esta rama de la que parten las demás conocidas como `feature`. Una vez que en develop ya tenemos subidas las diferentes funciones añadidas a través de las ramas feature y estamos listos para hacer una publicación, debemos crear una rama `release` a partir de `develop`. En este punto ya no podemos añadir nuevas funciones. En este nueva rama, solo se incluirán soluciones a errores, generación de documentación y demás tereas relacionadas con la próxima publicación del proyecto. Cuando tengamos lista la rama `release`, haremos merge con main y etiquetaremos con un número de versión. Posteriormente deberiámos volver a hacer merge con develop por si faltaran cosas por añadir desde que sacáramos la rama `release` de `develop`.  

### Comandos de git-flow  
Aunque no es necesario usarlos ni instalar la extensión. Se puede trabajar con git-flow con los comandos normales de git, recordemos que solamente es una manera de trabajar con git, de estructurar el trabajo y los avances que hacemos en él en un equipo mas o menos grande de trabajadores.  

**- GIT FLOW INIT**  
Con este comando podemos comenzar a usar git flow. Siempre se debe ejecutar en un repositorio ya existente. Lo que pasará será que se creará la rama `develop`, a partir de la rama `main`, una vez ejecutado este comando. Saldrá un menú en el que te pedirá que nombres como quieras a las ramas básicas de git flow.  

**comandos para las ramas feature**  

**- GIT FLOW FEATURE START**  
Con este comando creamos una nueva rama y nos movemos a ella automáticamente. Esta rama procede de `develop`:  

    -git flow feature start featureA  

**- GIT FLOW FEATURE FINISH**  
Una vez hayamos terminado el trabajo en la rama creada con el anterior comando y una vez hecho merge con `develop`, debemos ejecutar:  

    -git flow feature finish featureA  

**- GIT FLOW FEATURE PUBLISH**  
Nos permite publicar la rama creada en un repositorio remoto ya creado:

    -git flow feature publish featureA

**- GIT FLOW FEATURE PULL ORIGIN**  
Nos permite recuperar un repositorio publicado en un repositorio remoto:  

    -git flow feature pull origin featureA  

**comandos para las ramas release**  

**- GIT FLOW RELEASE START**  
Con este comando creamos una rama release, a partir de `develop`, como hemos explicado anteriormente.

    -git flow relase start 0.1.0  

Para iniciar la rama release/0.1.0 y cambiarnos a ella.  

**- GIT FLOW RELEASE FINISH**  
Para finalizar la rama `release` llamada 0.1.0: 

    -git flow release finish '0.1.0' 

Existen otras ramas en git flow como `hotfix`, son ramas de mantenimiento o corrección. Se utilizan para reparar posibles errores que puedan surgir en un proyecto en desarollo. Estas ramas deben salir de la rama main, reparar el error y volver a main etiquetando una nueva versión. También debería mergearse con la rama `develop` o `release` que tengamos activa. Es una manera de reparar errores sin interrumpir el flujo de trabajo en otras ramas como `feature` o `develop`.  

**Comandos para las ramas hotfix**  

**- GIT FLOW HOTFIX START**  
Como en las otras ramas, este sería el comando para crear una rama `hotfix` y cambiarse a ella:  

    -git flow hotfix start rama_hotfix

**- GIT FLOWHOTFIX FINISH**  
Y este sería el comando para terminar la rama `hotfix`, no sin antes ejecutar el comando merge tanto con `main` como con `develop` o `release`, dependiendo de cómo tengamos organizado en ese momento nuestro flujo de trabajo:  

    -git flow hotfix finish rama_hotfix

**Aclaraciones importantes**  

Antes de finalizar una rama con `git flow finish`, sea del tipo que sea hay que unirla, con el comando `git merge`.
Ejemplo de todos los comandos a ejecutar con una rama `hotfix`:  

    -git flow hotfix start rama_hotfix
    -git checkout main o master
    -git merge rama_hotfix
    -git checkout develop
    -git merge rama_hotfix
    -git branch -D rama_hotfix
    -git flow hotfix finish rama_hotfix

## Integración continua

### Explicación  
La integración contínua consiste es en esencia realizar de forma práctica la integración automática de cádigo de varios desarroyadores en un único proyecto de software, esto permite comprobar y fusionar mas habitualmente el código para, normalmente realizar comprobaciones, compilaciones y pruebas; la automatización se utiliza en sentido de comprobar o verificar la integridad del código antes de su integración.

### Cuando se utiliza
La CI se utiliza generalmente en conjunto con un flujo de trabajo de desarrollo de software de metodología ágil. Una unidad creará una hoja de ruta de tareas. A continuación, estas tareas se distribuyen entre los miembros del equipo de software para la entrega. Al utilizar la CI, estas tareas de desarrollo de software pueden desarrollarse independientemente y en paralelo entre los desarrolladores asignados. Una vez que una de estas tareas esté completa, un desarrollador introducirá el nuevo trabajo en el sistema de CI para integrarlo con el resto del proyecto.

### Ventajas de la CI
 * -Versatilidad
    - Su versatilidad se debe a que puede ser usado por un grupo unicamente de desarrolladores o a toda una organización con distintos sectores.
 * -Disposicion al escalado
    - La CI permite que las organizaciones escalen en tamaño del equipo, del código e infraestructura.
 * -Mejora la corrección y detección de errores
    - Los equipos de producto pueden probar las ideas e iterar los diseños del producto más rápido con una plataforma de CI optimizada.
 * -Mejora la comunicación
    -  permite una mayor colaboración entre el desarrollo y las operaciones en un equipo de desarrolladores

### Retos de la CI
  * -Adopción e instalación
    - puede suponer cierto esfuerzo si no se implementa en un grupo desde el primer momento, ya que se deberá reestructurar la infraestructura existente   
  * -Curva de aprendizaje
    - los sistemas de control de versiones, la infraestructura de alojamiento y las tecnologías de orquestación pueden suponer una inversión de curva de aprendizaje para el equipo. 

### Herramientas CI
* -AWS CodePipeline  
Esta es la herramienta de Integración Continua de Amazon Web Services, con alojamiento en la nube.  

* -GitLab  
GitLab se creó para mejorar la experiencia en GitHub, y funciona de manera muy similar a este solo que, con GitLab tienes acceso a su herramienta de integración continua con la creación de una cuenta. También es compatible con dockers. Al igual que el IC de AWS, se aloja en la nube, GitLab Runners es un ejemplo de *Runner* si se combina con GitLab CI.

* -Jenkins
A diferencia de las dos herramientas de IC anteriores esta está diseñada para su instalación en local. Muy utilizada en caso de empresas que tratan con datos confidenciales de clientes, por ejemplo.

### Runners, pipelines y stages
 * -Runners  
   - Los runners son máquinas virtuales aisladas que corren pasos predefinidos a través de una API 
 * -Pipelines  
   - En esencia aceleran el proceso de enviar runners en el proyecto 
 * -Stages
   - Build, test y deploy; estas son las tres stages que existen, se dividen en *build* en la cual se monta esencialmente la que seria la aplicación, después estaría el *test* en el cual se testearía la anteriormente montada *build* realizando test unitatios entre otros, por ultimo se realizaría un deploy, el cual consistiría en unir el código a la rama principal.